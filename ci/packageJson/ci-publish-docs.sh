#!/bin/bash

# Package
cd package
npm install
npm run build
cd ..

# Docs
export BASE_PATH="/use-state-validate"
cd docs
npm install
npm run build
cd ..

cp -r ./docs/dist ./public