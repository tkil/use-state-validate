/*
  This file will run build time (or on the server) to generate the shell of the HTML.
  Things that should go here:
  * Analytic Script
  * 3rd Party Script Setup
  * CFX Header / Footer

  Next.Js Docs: https://nextjs.org/docs/advanced-features/custom-document
*/

import { Html, Head, Main, NextScript } from "next/document"

export default function Document() {
  return (
    <Html lang="en">
      <Head>{/* ANALYTIC / MARKETING SCRIPTS GO HERE */}</Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
