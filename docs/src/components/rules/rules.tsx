import React from "react"
import { Layout } from "@components/shared/layout/layout"
import PropTable from "@components/propTable/propTable"
import Signature from "@components/shared/signature/signature"
import docsData from "@data/docs.json"
import CodeBlock from "@components/shared/codeBlock/codeBlock"
import { CodeBlockExample } from "@components/shared/codeBlockExample/codeBlockExample"

import { RuleOrderingExample } from "./examples/ruleOrdering"
import rawRuleOrderingExample from "!!raw-loader!./examples/ruleOrdering"

const formatMarkup = (str: string) => <span dangerouslySetInnerHTML={{ __html: str }} />

const formatProps = (props: any) =>
  props.map(({ name, type, description }: any) => ({
    name: name,
    type,
    description: formatMarkup(description),
  }))

export const Rules: React.FC = () => (
  <Layout>
    <section>
      <h2>Rules</h2>
      <p>{docsData.rules.description}</p>
      <CodeBlock language="typescript">{docsData.rules.descriptionExample}</CodeBlock>
      <p dangerouslySetInnerHTML={{ __html: docsData.rules.descriptionFollowup }} />

      <Signature title="Rule Tuple" code={docsData.rules.ruleTuple} />
    </section>

    <section>
      <h3>Rule Utils</h3>
      <p>
        A common theme of <strong>use-state-validation</strong> is composition. This keeps the library light while
        providing flexibility. Still a validation library wouldn't be terribly useful without some basic validation
        tools. With this in mind, commonly used rules are provided via rule generator functions that may be imported
        into your project.
      </p>
      <CodeBlock language="typescript">{docsData.rules.importExample}</CodeBlock>
      <PropTable heading={<strong>Rule Utils</strong>} props={formatProps(docsData.rules.utils)} />
    </section>

    <section>
      <h3>Rule Ordering</h3>
      <p>
        Rules should be ordered from broad to specific if you intend on only showing the first error to the user.
        Required will always be the first error as it is the most general. Guiding the user not to enter junk data may
        be useful for your first rules if you decide to get very nitty gritty with validation messaging. Messages like
        "no special characters allowed" is much more helpful to a user than stating the "the field is invalid".
      </p>
      <RuleOrderingExample />
      <CodeBlockExample>{rawRuleOrderingExample}</CodeBlockExample>
    </section>
  </Layout>
)

export default Rules
