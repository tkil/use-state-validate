import CodeBlockObject from "@components/shared/codeBlockObject/codeBlockObject"
import { Button, Form, Input } from "semantic-ui-react"
import useStateValidate, { ruleEmail, ruleMatch } from "use-state-validate"

export const RuleOrderingExample = () => {
  const field = useStateValidate("", {
    label: "What is the best food?",
    required: "No food isn't an option",
    cueDelay: 1250,
    rules: [
      //doc-format
      [ruleMatch(/^[A-Za-z0-9\ ]+$/), "Food doesn't have special characters..."],
      [ruleMatch(/^[A-Za-z\ ]+$/), "Food doesn't have numbers..."],
      [ruleMatch(/pizza/i), "I am thinking pizza..."],
      [ruleMatch(/pepperoni/i), "Yes... but maybe on pizza?"],
      [ruleMatch(/pepperoni pizza/i), "How about pepperoni pizza?"],
    ],
  })

  return (
    <Form>
      <Form.Group>
        <Form.Field
          control={Input}
          label={field.label}
          value={field.value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => field.setValue(e.target.value)}
          error={
            field.cueInvalid
              ? {
                  content: field.errors[0],
                  pointing: "above",
                }
              : undefined
          }
        />
      </Form.Group>
      <Button type="button" color="black" onClick={field.restore}>
        Clear
      </Button>
      <CodeBlockObject object={field} />
    </Form>
  )
}
