import styles from "./propTable.module.scss"
import type { FC, ReactNode } from "react"
import { Table } from "semantic-ui-react"

export interface IPropTableProps {
  heading: ReactNode
  props: { name: string; type: string; description: ReactNode; }[]
}

export const PropTable: FC<IPropTableProps> = ({ heading, props }) => {
  return (
    <Table celled striped className={styles.root}>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell colSpan="3" className={styles.header}>
            {heading}
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {props.map(({ description, name, type }) => (
          <Table.Row>
            <Table.Cell collapsing>{name}</Table.Cell>
            <Table.Cell collapsing>{type}</Table.Cell>
            <Table.Cell>{description}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  )
}

export default PropTable
