import styles from "./interface.module.scss"
import React from "react"
import { Layout } from "@components/shared/layout/layout"
import { Message } from "semantic-ui-react"
import PropTable from "@components/propTable/propTable"
import Signature from "@components/shared/signature/signature"
import docsData from "@data/docs.json"
import Link from "next/link"

const formatMarkup = (str: string) => <span dangerouslySetInnerHTML={{ __html: str }} />

const formatProps = (props: any) =>
  props.map(({ name, type, description }: any) => ({
    name: name,
    type,
    description: formatMarkup(description),
  }))

export const Interface: React.FC = () => (
  <Layout>
    <h2>Hook Interface</h2>

    <Signature code={docsData.signature} />

    {/* // >[REFACTOR]<: to definition component */}
    <Message className={styles.definition}>
      <Message.Header>
        <strong>initialValue {"<T>"}</strong>
      </Message.Header>
      <p>{docsData.api.initialValue}</p>
    </Message>

    <PropTable heading={<strong>fieldObject</strong>} props={formatProps(docsData.api.fieldObject)} />
    <PropTable heading={<strong>configObject</strong>} props={formatProps(docsData.api.configObject)} />
    See <Link href="/rules">Rules</Link> for a deeper dive into the rules prop.
  </Layout>
)

export default Interface
