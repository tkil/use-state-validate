import type { FC } from "react"
import Link from "next/link"
import { Layout } from "@components/shared/layout/layout"
import { CodeBlock } from "@components/shared/codeBlock/codeBlock"
import GettingStartedExample from "./examples/gettingStartedExample"
import rawGettingStartedExample from "!!raw-loader!./examples/gettingStartedExample"
import CodeBlockExample from "@components/shared/codeBlockExample/codeBlockExample"
import { Signature } from "@components/shared/signature/signature"
import docsData from "@data/docs.json"
import { Message } from "semantic-ui-react"
import { BUNDLEPHOBIA_LINK } from "@utils/constants"
import useStateValidate from "use-state-validate"

export const GettingStarted: FC = () => (
  <Layout>
    <section>
      <h2>Getting Started</h2>
      <p dangerouslySetInnerHTML={{ __html: docsData.description }}></p>
      <p>
        This package currently weighs in at: <a href={BUNDLEPHOBIA_LINK}>{docsData.size} (per Bundlephobia)</a>
      </p>
      <p>Install with NPM via:</p>
      <CodeBlock language="bash">npm install use-state-validate</CodeBlock>
      <Signature
        title="Hook Signature"
        code={docsData.signature}
        closing={
          <>
            See <Link href="/interface">API</Link> for full details.
          </>
        }
      />

      <h3>Basic Usage</h3>
      <Message>
        <GettingStartedExample />
      </Message>
      <CodeBlockExample>{rawGettingStartedExample}</CodeBlockExample>
      <h3>Next Steps</h3>
      <p>
        See the <Link href="/interface">Cuing</Link> page of the docs to get a feel for how the hook works and some of
        the validation patterns you can use. If you like what you see, do a proof of concept with the{" "}
        <strong>use-state-validate</strong> and explore the <Link href="/interface">API</Link>. If you still like what you
        see, dive into the <Link href="/composition">Composition</Link> guide to make your forms clean and lean.
      </p>
    </section>
  </Layout>
)

export default GettingStarted
