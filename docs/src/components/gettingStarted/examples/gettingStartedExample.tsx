import useStateValidate, { ruleEmail } from "use-state-validate"

export const MyComponent = () => {
  const email = useStateValidate("", {
    label: "Email",
    required: true,
    cueDelay: 1250,
    rules: [[ruleEmail(), "Email must be in a valid format"]],
  })

  return (
    <div>
      {/* Label */}
      <label htmlFor="email">{email.label}:</label>
      {/* Input */}
      <input
        id="email"
        value={email.value}
        aria-invalid={email.cueInvalid || undefined}
        onChange={(e) => email.setValue(e.target.value)}
      />
      {/* Error Message */}
      {email.cueInvalid && <p className="invalid" aria-live="polite">{email.errors[0]}</p>}
    </div>
  )
}

export default MyComponent
