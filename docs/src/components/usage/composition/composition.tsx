import type { FC } from "react"
import { Layout } from "@components/shared/layout/layout"
import CodeBlockExample from "@components/shared/codeBlockExample/codeBlockExample"
import CodeBlock from "@components/shared/codeBlock/codeBlock"
import { Grey, Red } from "@components/shared/colors/colors"

// Examples
import rawConfigBeside from "!!raw-loader!./examples/configBeside/myComponent"
import rawConfigBesideValidation from "!!raw-loader!./examples/configBeside/myComponent.validation"
import rawFieldBag from "!!raw-loader!./examples/fieldBag/myComponent"
import rawFieldBagWithValidation from "!!raw-loader!./examples/fieldBag/myComponentWithValidation"
import rawFieldBagTraversal from "!!raw-loader!./examples/fieldBagTraversal/myComponent"
import { BUNDLEPHOBIA_LINK } from "@utils/constants"
import docData from "@data/docs.json"

export interface IUsageCompositionProps {}

export const UsageComposition: FC<IUsageCompositionProps> = (props) => {
  return (
    <Layout>
      <section>
        <h2>Composition Patterns</h2>
        <p>
          To make the most of this hook, a collection of patterns are provided to make the job of managing complex forms
          easier. These are not mandatory, but are strongly encouraged.
        </p>
      </section>
      <section id="config-beside" data-fragment-offset>
        <h2>Config Beside</h2>
        <p>
          The second argument of <Red>use-state-validate</Red> is the rule object. Unfortunately this can make your
          component busy. A good strategy is to store your validation rule objects somewhere else. A good place for this
          is a sister file right beside your component. The validation rules are then imported in an injected into the
          rules parameter.
        </p>
        <p>
          By using this strategy, your hook should now be a single line. Better yet, we just created a separation of
          concerns and now validation rules live in a very specific home. Our logic is now declarative which greatly
          improves the readability of your code.
        </p>
        <h3>File Structure</h3>
        <CodeBlock language="bash">{`myComponent/
├─ myComponent.tsx
├─ myComponent.validation.tsx`}</CodeBlock>
        <h3>
          Validation Config <Grey> - myComponent.validation.tsx</Grey>
        </h3>
        <CodeBlockExample>{rawConfigBesideValidation}</CodeBlockExample>
        <h3>
          Component <Grey> - myComponent.tsx</Grey>
        </h3>
        <CodeBlockExample>{rawConfigBeside}</CodeBlockExample>
      </section>
      <section id="field-bag" data-fragment-offset>
        <h2>Field Bag</h2>
        <p>
          Storing each field in its own const may be fine for some use cases, but additional possibilities open up if
          these are stored in an object. Transportation is significantly easier as the entire fields object can be sent
          to a sub component or elsewhere.
        </p>
        <p>
          Prop drilling becomes much less of a problem and the temptation to reach out for a tool like Context API or
          Redux is mitigated. Most of the time these tools are overkill, but that conversation is outside the scope of
          these docs.
        </p>
        <p>Here is what the "Field Bag" pattern looks like:</p>
        <CodeBlockExample>{rawFieldBag}</CodeBlockExample>
        <p>Better yet, we can use the "Config Beside" pattern with the "Field Bag" pattern to keep things nice and tidy.</p>
        <CodeBlockExample>{rawFieldBagWithValidation}</CodeBlockExample>
      </section>
      <section id="field-traversal" data-fragment-offset>
        <h2>Field Bag Traversal</h2>
        <p>
          Another benefit of storing our fields in a "Field Bag" is the added ability to do traversal. We can create
          some pretty slick functions using object methods and array methods. Your logic for checking if "all fields are
          valid" can be accomplished in a few short lines of code. With a mere {" "}
          <a href={BUNDLEPHOBIA_LINK}>{docData.size}</a> NPM package and some clever coding, the proverbial form dragon
          has been slain 🙌.
        </p>
        <CodeBlockExample>{rawFieldBagTraversal}</CodeBlockExample>
        {/* <h3>Demo</h3>
        <FieldBagTraversal /> */}
      </section>
    </Layout>
  )
}

export default UsageComposition
