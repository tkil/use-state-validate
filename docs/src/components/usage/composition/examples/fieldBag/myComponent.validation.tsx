import { defineConfig, ruleEmail, ruleMatch, rulePhone } from "use-state-validate"

export const validations = {
  firstName: defineConfig({
    label: "First Name",
    required: true,
    rules: [[ruleMatch(/^[A-Za-z0-9 ]*$/), "First Name must not contain special characters"]],
  }),
  lastName: defineConfig({
    label: "Last Name",
    required: true,
    rules: [[ruleMatch(/^[A-Za-z0-9 ]*$/), "Last Name must not contain special characters"]],
  }),
  email: defineConfig({
    label: "Email",
    required: true,
    rules: [[ruleEmail()]],
  }),
  phone: defineConfig({
    label: "Phone",
    required: true,
    rules: [[rulePhone()]],
  }),
}
