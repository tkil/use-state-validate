import useStateValidate from "use-state-validate"
import SubComponent from "./subComponent"
import { validations } from "./myComponent.validation"

export const MyComponent = () => {
  const fieldBag = {
    firstName: useStateValidate("", validations.firstName),
    lastName: useStateValidate("", validations.lastName),
    email: useStateValidate("", validations.email),
    phone: useStateValidate("", validations.phone),
  }
  return (
    <div>
      <SubComponent fieldBag={fieldBag} />
    </div>
  )
}

export default MyComponent
