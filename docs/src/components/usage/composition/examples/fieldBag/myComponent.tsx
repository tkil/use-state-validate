import useStateValidate from "use-state-validate"
import SubComponent from "./subComponent"

export const MyComponent = () => {
  const fieldBag = {
    firstName: useStateValidate("", { label: "First Name", required: true }),
    lastName: useStateValidate("", { label: "Last Name", required: true }),
    email: useStateValidate("", { label: "Email", required: true }),
    phone: useStateValidate("", { label: "Phone", required: true }),
  }

  return (
    <div>
      <SubComponent fieldBag={fieldBag} />
    </div>
  )
}

export default MyComponent
