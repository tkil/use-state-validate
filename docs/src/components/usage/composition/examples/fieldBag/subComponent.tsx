import type { FC } from "react"

export interface ISubComponentProps {
  fieldBag: any
}

export const SubComponent: FC<ISubComponentProps> = () => null

export default SubComponent
