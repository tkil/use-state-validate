import useStateValidate from "use-state-validate"
import { validations } from "./myComponent.validation"

export const MyComponent = () => {
  const firstName = useStateValidate("", validations.firstName)
  const lastName = useStateValidate("", validations.lastName)
  const email = useStateValidate("", validations.email)
  const phone = useStateValidate("", validations.phone)

  return <>Write some form and UI stuff...</>
}
