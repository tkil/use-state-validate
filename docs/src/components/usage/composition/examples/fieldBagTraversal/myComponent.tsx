import { Button } from "semantic-ui-react"
import useStateValidate, {
  fieldBagChanged,
  fieldBagReduceToValues,
  fieldBagTraverse,
  fieldBagValid,
} from "use-state-validate"
import { validations } from "./myComponent.validation"

export const MyComponent = () => {
  const fieldBag = {
    firstName: useStateValidate("", validations.firstName),
    lastName: useStateValidate("", validations.lastName),
    email: useStateValidate("", validations.email),
    phone: useStateValidate("", validations.phone),
  }
  const fieldValues = fieldBagReduceToValues(fieldBag)

  // To iterate over all fields, this use case would cue all fields for the user:
  const cueAllFields = () => fieldBagTraverse(fieldBag, ({ setCue }) => setCue(true))

  const codeBlockStyles = {
    background: "#282C34",
    color: "#ABB2BF",
    padding: "8px 16px",
  }

  return (
    <div>
      <div>UI</div>
      <Button>Cue All</Button>

      <h5>Raw Values</h5>
      <pre style={codeBlockStyles}>
        <code>{JSON.stringify(fieldValues, null, 2)}</code>
      </pre>

      <h5>Is Form Valid</h5>
      <pre style={codeBlockStyles}>
        <code>Is Form Valid: {fieldBagValid(fieldBag)}</code>
      </pre>
  
      <h5>Is Form Valid</h5>
      <pre style={codeBlockStyles}>
        <code>Was Form Change: {fieldBagChanged(fieldBag)}</code>
      </pre>

      <h5>Fields Object</h5>
      <pre style={codeBlockStyles}>
        <code>{JSON.stringify(fieldBag, null, 2)}</code>
      </pre>
    </div>
  )
}

export default MyComponent
