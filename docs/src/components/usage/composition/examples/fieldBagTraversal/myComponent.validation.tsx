export const validations = {
  firstName: {
    label: "First Name",
    required: true,
    match: /^[A-Za-z0-9 ]*$/,
    message: {
      match: "First Name must not contain special characters",
    },
  },
  lastName: {
    label: "Last Name",
    required: true,
    match: /^[A-Za-z0-9 ]*$/,
    message: {
      match: "Last Name must not contain special characters",
    },
  },
  email: {
    label: "Email",
    required: true,
    match: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, // Stolen from https://www.abstractapi.com/tools/email-regex-guide
  },
  phone: {
    label: "Phone",
    required: true,
    match: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/, // Stolen from https://ihateregex.io/expr/phone/
  },
}
