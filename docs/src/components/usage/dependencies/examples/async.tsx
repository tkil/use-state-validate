import useStateValidate from "use-state-validate"
import { Button, Form, Input } from "semantic-ui-react"
import { useEffect, useState } from "react"
import { CodeBlockObject } from "@components/shared/codeBlockObject/codeBlockObject"

export const AsyncExample = () => {
  const [answerResponse, setAnswerResponse] = useState("")
  const field = useStateValidate("", {
    rules: [[() => /correct/i.test(answerResponse), answerResponse]],
    deps: [answerResponse],
    cueDelay: 10_000,
  })
  useEffect(() => {
    simulatedService(field.value).then((result) => {
      setAnswerResponse(result)
      field.setCue(true)
    })
  }, [field.value])

  return (
    <>
      {/* >[DEBUG]< */}
      <div
        style={{
          minHeight: "4rem",
          backgroundColor: "lightyellow",
          padding: "0.5rem",
          boxShadow: "3px 3px 3px lightgrey",
          borderRadius: "4px",
        }}
      >
        <h3>[DEBUG]</h3>
        <pre>{JSON.stringify(field, null, 2)}</pre>
      </div>
      {/* >[DEBUG]< */}
      <Form>
        <Form.Group>
          <Form.Field
            control={Input}
            label={field.label}
            value={field.value}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => field.setValue(e.target.value)}
            error={
              field.cueInvalid
                ? {
                    content: field.errors[0],
                    pointing: "above",
                  }
                : undefined
            }
          />
        </Form.Group>
        <Button type="submit" color="blue">
          Submit
        </Button>
        <Button type="button" color="black" onClick={field.restore}>
          Clear
        </Button>
        <CodeBlockObject object={field} />
      </Form>
    </>
  )
}

const simulatedService = (answer: string) =>
  new Promise((res: (value: string) => void) => {
    setTimeout(() => {
      res(answer === "42" ? "Correct!" : "Invalid: That is not the answer to everything!")
    }, 1_250)
  })
