import CodeBlockObject from "@components/shared/codeBlockObject/codeBlockObject"
import { Button, Form, Input, Label, Radio } from "semantic-ui-react"
import useStateValidate, { ruleEnum } from "use-state-validate"

const PIZZA_OPTIONS = ["pepperoni", "cheese", "veggie", "other"]

export const ConditionalRequiredExample = () => {
  const field1 = useStateValidate("", {
    label: "Favorite Pizza",
    required: "Must choose a type of pizza",
    cueDelay: 1250,
  })

  const field2 = useStateValidate("", {
    label: "Other",
    required: field1.value === "other" ? "What kind of other special pizza do you enjoy?" : undefined,
    rules: [
      //doc-format
      [(value) => !ruleEnum(PIZZA_OPTIONS)(value), "Hey, we gave that to you as an option!"],
    ],
    deps: [field1.value],
    cueDelay: 1250,
  })

  return (
    <Form
      onSubmit={() => {
        field1.setCue(true)
        field2.setCue(true)
      }}
      noValidate // skips HTML5 validation so use-state-validate can do it
    >
      <Form.Group style={{ gap: "3rem" }}>
        <Form.Field></Form.Field>
        <Form.Field>
          Choose your favorite pizza:
          {PIZZA_OPTIONS.map((opt) => (
            <Form.Field>
              <Radio
                key={opt}
                label={opt}
                name="radioGroup"
                value={opt}
                checked={field1.value === opt}
                onChange={() => {
                  field1.setValue(opt)
                  field2.restore()
                }}
              />
            </Form.Field>
          ))}
          {field1.cueInvalid && (
            <Label pointing prompt>
              {field1.errors[0]}
            </Label>
          )}
        </Form.Field>
        <Form.Field
          disabled={field1.value !== "other"}
          control={Input}
          required={field2.required}
          label={field2.label}
          value={field2.value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => field2.setValue(e.target.value)}
          error={
            field2.cueInvalid
              ? {
                  content: field2.errors[0],
                  pointing: "above",
                }
              : undefined
          }
        />
      </Form.Group>
      <Button type="submit" color="blue">
        Submit
      </Button>
      <Button
        type="button"
        color="black"
        onClick={() => {
          field1.restore()
          field2.restore()
        }}
      >
        Clear
      </Button>
      <CodeBlockObject object={field1} object2={field2} />
    </Form>
  )
}
