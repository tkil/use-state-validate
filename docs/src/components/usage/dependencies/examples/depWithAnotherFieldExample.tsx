import CodeBlockObject from "@components/shared/codeBlockObject/codeBlockObject"
import { Button, Form, Input } from "semantic-ui-react"
import useStateValidate, { ruleEmail } from "use-state-validate"

export const DepWithAnotherFieldExample = () => {
  const field1 = useStateValidate("", {
    label: "Email",
    required: true,
    cueDelay: 1250,
    rules: [[ruleEmail(), "Email must be in a valid format"]],
  })

  const field2 = useStateValidate("", {
    label: "Confirm Email",
    required: true,
    cueDelay: 1250,
    deps: [field1.value],
    rules: [[(value: string) => value === field1.value, "Email must match"]],
  })

  return (
    <Form>
      <Form.Group>
        <Form.Field
          control={Input}
          label={field1.label}
          value={field1.value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => field1.setValue(e.target.value)}
          error={
            field1.cueInvalid
              ? {
                  content: field1.errors[0],
                  pointing: "above",
                }
              : undefined
          }
        />
        <Form.Field
          control={Input}
          label={field2.label}
          value={field2.value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => field2.setValue(e.target.value)}
          error={
            field2.cueInvalid
              ? {
                  content: field2.errors[0],
                  pointing: "above",
                }
              : undefined
          }
        />
      </Form.Group>
      <Button
        type="button"
        color="black"
        onClick={() => {
          field1.restore()
          field2.restore()
        }}
      >
        Clear
      </Button>
      <CodeBlockObject object={field1} object2={field2} />
    </Form>
  )
}
