import type { FC } from "react"
import { Layout } from "@components/shared/layout/layout"
import CodeBlockExample from "@components/shared/codeBlockExample/codeBlockExample"

// Examples
import { DepWithAnotherFieldExample } from "./examples/depWithAnotherFieldExample"
import rawDepWithAnotherFieldExample from "!!raw-loader!./examples/depWithAnotherFieldExample"
import { ConditionalRequiredExample } from "./examples/conditionalRequired"
import rawConditionalRequiredExample from "!!raw-loader!./examples/conditionalRequired"

// import { AsyncExample } from "./examples/async"
// import rawAsyncExample from "!!raw-loader!./examples/async"
export interface IUsageCuingProps {}

export const UsageDependencies: FC<IUsageCuingProps> = () => {
  return (
    <Layout>
      <section>
        <h2>Dependencies</h2>
        <p>
          Sometimes validation is dependent on sister fields or external values. <strong>use-state-validate</strong> is
          able to handle these scenarios by using the <strong>dep</strong> config option.
        </p>
      </section>

      <section id="another-field" data-fragment-offset>
        <h2>Dependency with Another Field</h2>
        <p>
          When comparing one field against another such as email confirmation, <strong>deps</strong> is your friend.
          Whenever a value changes in the deps array, the validation will reevaluate.
        </p>
        <DepWithAnotherFieldExample />
        <CodeBlockExample>{rawDepWithAnotherFieldExample}</CodeBlockExample>
      </section>

      <section id="conditional-required" data-fragment-offset>
        <h2>Conditional Required</h2>
        <p>
          There may be a scenario in which sometimes a field is required, but not always. The <strong>required</strong>{" "}
          config option can accept an expression. If a value in the expression is base on a mutable value, tell{" "}
          <strong>deps</strong> about it, so it may update on re-renders.
        </p>
        <ConditionalRequiredExample />
        <CodeBlockExample>{rawConditionalRequiredExample}</CodeBlockExample>
      </section>
    </Layout>
  )
}

export default UsageDependencies
