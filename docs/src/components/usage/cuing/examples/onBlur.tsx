import CodeBlockObject from "@components/shared/codeBlockObject/codeBlockObject"
import { Button, Form, Input } from "semantic-ui-react"
import useStateValidate, { ruleEmail } from "use-state-validate"

export const OnBlurExample = () => {
  const field = useStateValidate("", {
    label: "Email",
    required: true,
    rules: [[ruleEmail(), "Email must be in a valid format"]],
  })

  return (
    <Form>
      <Form.Group>
        <Form.Field
          control={Input}
          label={field.label}
          value={field.value}
          onBlur={() => field.setCue(true)}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => field.setValue(e.target.value)}
          error={
            field.cueInvalid
              ? {
                  content: field.errors[0],
                  pointing: "above",
                }
              : undefined
          }
        />
      </Form.Group>
      <Button
        type="button"
        color="black"
        onClick={field.restore}
      >
        Clear
      </Button>
      <CodeBlockObject object={field} />
    </Form>
  )
}
