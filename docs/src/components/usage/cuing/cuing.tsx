import type { FC } from "react"
import { Layout } from "@components/shared/layout/layout"
import CodeBlockExample from "@components/shared/codeBlockExample/codeBlockExample"
// Examples
// import { DependentExample } from "./examples/dependent"
// import rawDependentExample from "!!raw-loader!./examples/dependent"
import { OnSetExample } from "./examples/onSet"
import rawOnSetExample from "!!raw-loader!./examples/onSet"
import { OnBlurExample } from "./examples/onBlur"
import rawOnBlurExample from "!!raw-loader!./examples/onBlur"
import { OnSubmitExample } from "./examples/onSubmit"
import rawOnSubmitExample from "!!raw-loader!./examples/onSubmit"
import { OnDebounceExample } from "./examples/onDebounce"
import rawOnDebounceExample from "!!raw-loader!./examples/onDebounce"

export interface IUsageCuingProps {}

export const UsageCuing: FC<IUsageCuingProps> = () => {
  return (
    <Layout>
      <section>
        <h2>Cuing Patterns</h2>
        <p>
          Validation is just one part of the equation. A value being invalid vs telling a user that a value is invalid
          are two separate concerns. For this reason, the library uses "cuing" as a core concept and tackles the problem head
          on. The <strong>use-state-validate</strong> hook manages this with the <strong>cue</strong> boolean flag. This
          is intentionally kept separate from <strong>valid</strong> as it is the source of absolute validation truth.
        </p>
        <p>
          There is not a one size fits all solution for how to cue a user. Sometimes the pattern is in the control of the
          developer, but sometimes this is dictated by a UX designer or the business team. For this reason, tools are
          provided to handle a range of cuing patterns. You could argue that some options are better than others, but
          the important thing is that the choice is yours.
        </p>
        <p>
          The flags <strong>cue</strong> and <strong>valid</strong> should be used together to present validation
          messaging. For developer convenience, <strong>cueInvalid</strong> is provided as an alias and is equal to: <b>cue && !invalid</b>.
        </p>
      </section>
      <section id="on-set" data-fragment-offset>
        <h2>On Set</h2>
        <p>
          The first option for cuing is the most aggressive. The moment a field is updated, cue is set. To accomplish
          this, set <strong>cueDelay</strong> to 0.
        </p>
        <OnSetExample />
        <CodeBlockExample>{rawOnSetExample}</CodeBlockExample>
      </section>
      <section id="on-blur" data-fragment-offset>
        <h2>On Blur</h2>
        <p>
          Cue on blur has the benefit of not prematurely messaging users. When a user leaves a field and it loses focus,
          the user is cued if they made a mistake. To create this behavior, <strong>setCue(true)</strong> should be wired into
          the onBlur event.
        </p>
        <OnBlurExample />
        <CodeBlockExample>{rawOnBlurExample}</CodeBlockExample>
      </section>
      <section id="on-submit" data-fragment-offset>
        <h2>On Submit</h2>
        <p>
          Cue on submit leaves the user alone until the last possible moment when the form is submitted. To create this
          behavior, <strong>setCue(true)</strong> should be wired into the onSubmit event of the form.
        </p>
        <OnSubmitExample />
        <CodeBlockExample>{rawOnSubmitExample}</CodeBlockExample>
      </section>
      <section id="on-debounce" data-fragment-offset>
        <h2>On Debounce</h2>
        <p>
          Cue on debounce provides quick feedback without prematurely showing error text. The debounce option suppresses
          cuing when a user is updating a field. After a defined time delay, cue is set to true. When the user starts to
          make their corrections, cuing is suppressed again until the delay threshold is reached again.
        </p>
        <p>
          To achieve this behavior, set <strong>cueDelay</strong> to a value greater than 0. The value is numeric and
          expressed in milliseconds. Something in the range of 1000 to 2000 is a good starting point.
        </p>
        <OnDebounceExample />
        <CodeBlockExample>{rawOnDebounceExample}</CodeBlockExample>
      </section>


      {/* <section id="dependent" data-fragment-offset>
        <h2>Dependent</h2>
        <p>
        </p>
        <p>
        </p>
        <DependentExample />
        <CodeBlockExample>{rawDependentExample}</CodeBlockExample>
      </section> */}
    </Layout>
  )
}

export default UsageCuing
