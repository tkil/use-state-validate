import styles from "./signature.module.scss"

import type { FC, ReactNode } from "react"
import { Message } from "semantic-ui-react"

export interface SignatureProps {
  closing?: ReactNode
  code: string
  title?: string
}

export const Signature: FC<SignatureProps> = ({ closing, code, title }) => (
  <div className={styles.root}>
    {title && <h3>{title}</h3>}
    <Message className={styles.definition}>
      <pre className={styles.code}>
        <code dangerouslySetInnerHTML={{ __html: code }} />
      </pre>
    </Message>
    {closing}
  </div>
)

export default Signature
