import styles from "./header.module.scss"

import type { FC } from "react"
import { Button, Header } from "semantic-ui-react"
import Link from "next/link"
import pkg from "use-state-validate/package.json"
import docData from "@data/docs.json"
import { BUNDLEPHOBIA_LINK } from "@utils/constants"
import { AiOutlineMenu } from "@icons"

interface IAppHeaderProps {
  isDesktop: boolean
  sidebarVisible: boolean
  setSidebarVisible: (sidebarVisible: boolean) => void
}

export const AppHeader: FC<IAppHeaderProps> = ({ isDesktop, setSidebarVisible, sidebarVisible }) => (
  <Header as="h1" className={styles.root}>
    {!isDesktop && (
      <Button onClick={() => setSidebarVisible(!sidebarVisible)}>
        <AiOutlineMenu />
      </Button>
    )}
    <div>
      <h1 className={styles.title}>
        <Link href="/">{`${pkg.name}@${pkg.version}`}</Link>
      </h1>
      <div className={styles.description}>
        {pkg.description} - <a href={BUNDLEPHOBIA_LINK}>{docData.size}</a> ( 🌳 Tree shakable )
      </div>
    </div>
  </Header>
)

export default AppHeader
