import styles from "./nav.module.scss"

import type { FC } from "react"
import Link from "next/link"

export const Nav: FC = () => (
  <div className={styles.root}>
    <ul>
      <li>
        <Link href="/">Getting started</Link>
      </li>
      <li>
        <Link href="/principles">Principles</Link>
      </li>
      <li>
        <Link href="/interface">API</Link>
      </li>
      <li>
        <Link href="/rules">Rules</Link>
      </li>
      <li>
        <Link href="/cuing">Cuing</Link>
        <ul>
          <li>
            <Link href="/cuing#on-set">On Set</Link>
          </li>
          <li>
            <Link href="/cuing#on-blur">On Blur</Link>
          </li>
          <li>
            <Link href="/cuing#on-submit">On Submit</Link>
          </li>
          <li>
            <Link href="/cuing#on-debounce">On Debounce</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link href="/dependencies">Dependencies</Link>
        <ul>
          <li>
            <Link href="/dependencies#another-field">Another Field</Link>
          </li>
          <li>
            <Link href="/dependencies#conditional-required">Conditional Required</Link>
          </li>
        </ul>
      </li>
      <li>
        <Link href="/composition">Composition</Link>
        <ul>
          <li>
            <Link href="/composition#config-beside">Config Beside</Link>
          </li>
          <li>
            <Link href="/composition#field-bag">Field Bag</Link>
          </li>
          <li>
            <Link href="/composition#field-traversal">Field Traversal</Link>
          </li>
          {/* <li>
            <Link href="/composition#form-group">Form Group</Link>
          </li> */}
        </ul>
      </li>
    </ul>
  </div>
)

export default Nav
