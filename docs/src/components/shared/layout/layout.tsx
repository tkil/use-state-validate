import styles from "./layout.module.scss"

import AppHeader from "@components/shared/layout/header/header"
import { FC, ReactNode, useState } from "react"
import { Nav } from "./nav/nav"
import { Sidebar, Segment, Menu } from "semantic-ui-react"
import { useDeviceWidth } from "@utils/hook/useDeviceWidth"
import { MIN_DESKTOP_WIDTH } from "@utils/constants"

interface ILayoutProps {
  children: ReactNode
}

export const Layout: FC<ILayoutProps> = ({ children }) => {
  const deviceWidth = useDeviceWidth()
  const [sidebarVisible, setSidebarVisible] = useState(false)

  const isDesktop = deviceWidth >= MIN_DESKTOP_WIDTH

  return (
    <>
      <AppHeader isDesktop={isDesktop} setSidebarVisible={setSidebarVisible} sidebarVisible={sidebarVisible} />
      <div className={styles.body}>
        <Sidebar.Pushable as={Segment}>
          <Sidebar
            animation={isDesktop ? "overlay" : "overlay"}
            as={Menu}
            className={styles.sidebar}
            icon="labeled"
            inverted
            onClick={() => setSidebarVisible(false)}
            onHide={() => setSidebarVisible(false)}
            vertical
            visible={isDesktop || sidebarVisible}
            width="thin"
          >
            <div className={styles.navContainer}>
              <Nav />
            </div>
          </Sidebar>
          {/* <Sidebar.Pusher> */}
          <main className={styles.content}>{children}</main>
          {/* </Sidebar.Pusher> */}
        </Sidebar.Pushable>
      </div>
    </>
  )
}

export default Layout
