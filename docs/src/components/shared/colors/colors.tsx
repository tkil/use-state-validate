import styles from "./colors.module.scss"

import type { FC, ReactNode } from "react"

export interface IColorProps {
  bold?: boolean
  children: ReactNode
}

export const Red: FC<IColorProps> = ({ bold, children }) => (
  <span className={styles.red} data-bold={bold || undefined}>
    {children}
  </span>
)

export const Grey: FC<IColorProps> = ({ bold, children }) => (
  <span className={styles.grey} data-bold={bold || undefined}>
    {children}
  </span>
)
