import type { FC } from "react"
import CodeBlock from "@components/shared/codeBlock/codeBlock"

export interface ICodeBlockExampleProps {
  children: string
}

const HIDDEN_PATTERNS = [
  /^.*\/\/doc-format.*$\n/gm,
  /^.*CodeBlockObject.*$\n/gm,
  /^.*data-code-block.*$\n/gm,
]

export const CodeBlockExample: FC<ICodeBlockExampleProps> = ({ children }) => (
  <CodeBlock language="typescript">{hideLinesFromExample(children)}</CodeBlock>
)

const hideLinesFromExample = (str: string) =>
  HIDDEN_PATTERNS.reduce((acc, pattern) => {
    return acc.replace(pattern, "")
  }, str)

export default CodeBlockExample
