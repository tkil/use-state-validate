import styles from "./codeBlock.module.scss"

import Highlight from "react-syntax-highlighter"
import atomDarkOne from "react-syntax-highlighter/dist/cjs/styles/hljs/atom-one-dark"
import { useState } from "react"
import { Button } from "semantic-ui-react"

export interface CodeBlockProps {
  children: string
  format?: boolean
  copyButton?: boolean
  language: string
}

export const CodeBlock: React.FC<CodeBlockProps> = ({ children, copyButton, format, language }) => {
  const [copied, setCopied] = useState(false)

  const handleCopyClick = () => {
    navigator.clipboard.writeText(children)
    setCopied(true)

    const timeout = setTimeout(() => {
      clearTimeout(timeout)
      setCopied(false)
    }, 2000)
  }

  const commonProps = {
    type: "button" as "button",
    className: styles.copyButton,
    onClick: handleCopyClick,
  }

  return (
    <div className={styles.root} data-copy-button={copyButton || undefined}>
      {copyButton && (
        <div className={styles.copyButtonWrapper}>
          {copied ? (
            <Button {...commonProps} data-copied="true">
              Copied!
            </Button>
          ) : (
            <button {...commonProps} onClick={handleCopyClick}>
              Copy to Clipboard
            </button>
          )}
        </div>
      )}
      <Highlight
        customStyle={{
          fontSize: "16px",
          lineHeight: "22px",
          padding: "16px",
          wordBreak: "break-all",
          whiteSpace: "pre-wrap",
          boxShadow: "0px 2px 4px rgba(50,50,93,.1)",
        }}
        language={language}
        lineProps={{ style: { wordBreak: "break-all", whiteSpace: "pre-wrap" } }}
        style={atomDarkOne}
        showInlineLineNumbers={true}
        lineNumberStyle={{
          color: "#5dc79e",
          textAlign: "right",
          marginLeft: "-0.5em",
          background: "#f2feef",
          padding: "0 10px 0 10px",
        }}
        wrapLines
      >
        {children}
      </Highlight>
    </div>
  )
}

export default CodeBlock
