import type { FC } from "react"
import CodeBlock from "@components/shared/codeBlock/codeBlock"

export interface ICodeBlockObjectProps {
  object: object
  object2?: object
}

export const CodeBlockObject: FC<ICodeBlockObjectProps> = ({ object, object2 }) =>
  object2 ? (
    <div data-code-block style={{ display: "flex", gap: "1rem" }}>
      <CodeBlock language="json">{JSON.stringify(object, null, 2)}</CodeBlock>
      <CodeBlock language="json">{JSON.stringify(object2, null, 2)}</CodeBlock>
    </div>
  ) : (
    <CodeBlock language="json">{JSON.stringify(object, null, 2)}</CodeBlock>
  )

export default CodeBlockObject
