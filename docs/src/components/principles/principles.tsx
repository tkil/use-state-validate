import type { FC } from "react"
import { Layout } from "@components/shared/layout/layout"
import { BUNDLEPHOBIA_LINK } from "@utils/constants"
import docData from "@data/docs.json"
import Link from "next/link"

export const Principles: FC = () => (
  <Layout>
    <h2>Principles</h2>
    <p>
      <strong>use-state-validate</strong> hook aims to do one thing and one thing well: manage validation state. A quick
      skim through these docs should get you up and running. Alternative form libraries require you commit significant
      time to both learn and configure. Tools should be solving problems, not introducing new ones.
    </p>

    <h3>Simple & Intuitive</h3>
    <p>
      At it's core, this util is a useState hook with validation capabilities. Rules go in, the value and validation
      flags come out. That is it. The tool doesn't try to be anything more and it doesn't over-engineer a solution for
      form validation.
    </p>

    <h3>Clean</h3>
    <p>
      Composition patterns in these docs are provided to help keep your logic tidy. This package is stand alone and does
      not suffer the dependency hell of being part of an ecosystem. Suggested patterns like{" "}
      <Link href="/composition#config-beside">Config Beside</Link> encourage declarative programming and separation of
      concerns for optimal readability.
    </p>

    <h3>Featherweight</h3>
    <p>
      This hook weighs in at <a href={BUNDLEPHOBIA_LINK}>{docData.size}</a>. It is free from 3rd party deps and readily tree
      shakes. Most validation offerings in the React ecosystem have very large and frankly unacceptable bundle sizes.
      Large packages hurt performance and library authors have a duty to be respectful of this and minimize footprint
      while providing value.
    </p>
  </Layout>
)

export default Principles
