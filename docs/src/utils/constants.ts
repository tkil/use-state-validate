import pkg from "../../../package/package.json"

export const BUNDLEPHOBIA_LINK = `https://bundlephobia.com/package/${pkg.name}@${pkg.version}`

export const DEFAULT_DEVICE_WIDTH = 400

export const MIN_DESKTOP_WIDTH = 900
