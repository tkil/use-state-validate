import { DEFAULT_DEVICE_WIDTH } from "@utils/constants"
import { useEffect, useState } from "react"

export const useDeviceWidth = (): number => {
  const [deviceWidth, setDeviceWidth] = useState(DEFAULT_DEVICE_WIDTH)

  useEffect(() => {
    const resizeCallback = () => setDeviceWidth(window.innerWidth)
    resizeCallback()
    window.addEventListener("resize", resizeCallback)
    return () => {
      window.removeEventListener("resize", resizeCallback)
    }
  }, [])

  return deviceWidth
}
