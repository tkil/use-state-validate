/**
 * @typedef {import("next").NextConfig} NextConfig
 * @typedef {import("webpack").Configuration} WebpackConfig
 */
const path = require("path")

/** @type {NextConfig} */
const nextConfig = {
  basePath: process.env.BASE_PATH,
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, "src", "styles")],
    prependData: `@import "modulePrepend.scss";`,
  },
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // /** @type {WebpackConfig} */ config.plugins.push(

    // )
    // Important: return the modified config

    return {
      ...config,

      resolve: {
        ...config.resolve,
        alias: {
          ...config.resolve.alias,
          // Work around for the dreaded multi versions of react hook error
          react: path.resolve("./node_modules/react"),
        },
      },
    }
  },
}

module.exports = nextConfig
