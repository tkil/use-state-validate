# use-state-validate

[![Version npm](https://img.shields.io/npm/v/use-state-validate.svg?style=flat-square)](https://www.npmjs.com/package/use-state-validate)

[![NPM](https://nodei.co/npm/use-state-validate.png?downloads=true&downloadRank=true)](https://nodei.co/npm/use-state-validate/)

#### Repo: https://gitlab.com/tkil/use-state-validate

<br />

## 📖 [Documentation](https://tkil.gitlab.io/use-state-validate) 📖

<br />

## Install
``` bash
# npm
npm install use-state-validate
# yarn
yarn add use-state-validate
# pnpm
pnpm install use-state-validate
```

``` js
  import useStateValidate from 'use-state-validate'; // ESM
  // or
  const useStateValidate = require('use-state-validate'); // CJS
```

## Summary
This package is a custom hook that extends useState to provide validation. <strong>use-state-validate</strong> is strictly focused on the validation of field values and does not try to handle every facet of form logic. Great effort was spent to make the utility razor thin to help you keep your overall bundle size small.

## Basic Use
```js
import useStateValidate, { ruleEmail } from "use-state-validate"

export const SomeComponent = () => {
  const field = useStateValidate("", {
    label: "Email",
    required: true,
    cueDelay: 0,
    rules: [[ruleEmail(), "Email must be in a valid format"]],
  })

  return (
      <form>
        <label htmlFor="email">Email</label>
        <input
          id="email"
          aria-invalid={field.cueInvalid || undefined}
          value={field.value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => field.setValue(e.target.value)}
        />
        {field.cueInvalid && <div aria-live="polite">{field.errors[0]}</div>}
      </form>
  )
}
```


See [Documentation](https://tkil.gitlab.io/use-state-validate) for full usage and API.

Package size @ [Bundlephobia](https://bundlephobia.com/package/use-state-validate)

### Changelog
* v3.0.0 - Overhaul, leans heavy on cuing concepts for smooth UX.  Lib made leaner and more flexible.
* v2.5.4 - Bugfix - required, message type
* v2.5.1 - Bugfix
* v2.4.0 - Adds name to IStateValidateRules and validation wrapper
* v2.3.0 - Exposes IStateValidateRules
* v2.2.0 - Adds types to rules object
* v2.1.0 - Adds types to validation wrapper
* v2.0.3 - React version bugfix
* v2.0.0 - Drops 3rd party dep and this project does its own validation
* v1.0.6 - No breaking changes, but departs from 3rd party dep
* v1.0.5 - Fixes bug where clean used after set does not honor set's mutation
* v1.0.1 - v1.0.4 - Readme updates - no code changes
* v1.0.0 - Hook has been stable in projects, bumping to initial release
* v0.0.x - Initial dev release - ASSUME UNSTABLE
