//@ts-check
"use strict"
const { readFile, writeFile } = require("fs/promises")
const path = require("path")
const pkg = require("../package.json")

const DIR_ROOT = path.resolve(__dirname, "..", "..")
const docsJson = path.join(DIR_ROOT, "docs", "src", "data", "docs.json")
const ORIGIN_BUNDLEPHOBIA = "https://bundlephobia.com"

/** @param {string} size */
const writeSizeDataToDocsJson = async (size) => {
  const docsJsonData = JSON.parse(await readFile(docsJson, "utf-8"))
  docsJsonData.size = size
  docsJsonData.name = pkg.name
  docsJsonData.version = pkg.version
  await writeFile(docsJson, JSON.stringify(docsJsonData, null, 2))
}

/**
 * @param {string} packageName
 * @param {string} packageVersion
 */
const fetchBundlephobiaSize = async (packageName, packageVersion) => {
  const { size } = await (
    await fetch(`${ORIGIN_BUNDLEPHOBIA}/api/size?package=${packageName}@${packageVersion}`)
  ).json()
  return `${Math.floor(size / 100) / 10}kB`
}

const main = async () => {
  const size = await fetchBundlephobiaSize(pkg.name, pkg.version)
  await writeSizeDataToDocsJson(size )
}
main()
