#!/bin/bash
set -e

npm run clean

# MJS
tsc -p tsconfig.mjs.json
echo "{ \"type\": \"module\" }" > ./lib/mjs/package.json
uglifyjs --compress --mangle --output ./lib/mjs/index.js -- ./lib/mjs/index.js


# CJS
tsc -p tsconfig.cjs.json
echo "{ \"type\": \"commonjs\" }" > ./lib/cjs/package.json
uglifyjs --compress --mangle --output ./lib/cjs/index.js -- ./lib/cjs/index.js

# Make note of package size
node ./ci/recordSize.js
