import type { DependencyList, ReactNode } from "react"
import { useEffect, useRef, useState } from "react"

type IFieldObjectCustomConfig<T, P> = Pick<P, Exclude<keyof P, keyof IFieldValidationConfigBase<T>>>

interface IFieldValidationStateBase<T> {
  changed: boolean
  cue: boolean
  cueInvalid: boolean
  errors: ReactNode[]
  label?: string
  required: boolean
  valid: boolean
  value: T
}

interface IFieldObjectBase<T> extends IFieldValidationStateBase<T> {
  restore: () => void
  setCue: (cue: boolean) => void
  setValue: (newValue: T) => void
}

export type IFieldObject<T, P = {}> = IFieldObjectCustomConfig<T, P> & IFieldObjectBase<T>

export type RuleCallback<T> = (value: T) => boolean

interface IFieldValidationConfigBase<T> {
  cueDelay?: number
  deps?: DependencyList
  label?: string
  required?: boolean | ReactNode
  rules?: [RuleCallback<T>, ReactNode?][]
}

export type IFieldValidationConfig<T, P = {}> = P & IFieldValidationConfigBase<T>

// --------------------------------------------------
// Main Hook
// --------------------------------------------------
export const useStateValidate = <T, P extends IFieldValidationConfigBase<T>>(initial: T, config: P): IFieldObject<T, P> => {
  const [state, setState] = useState(deriveFieldObject(initial, initial, config))
  const refState = useRef(state)
  const refMounted = useRef(false)
  let refTimeoutId = useRef<number>()

  const clearCueTimeout = () => clearTimeout(refTimeoutId.current as number)

  const setStateWithRef = (partialState: Partial<IFieldValidationStateBase<T>>) => {
    refState.current = {
      ...refState.current,
      ...partialState
    }
    setState(refState.current)
  }

  const restore = () => {
    clearCueTimeout()
    setStateWithRef(deriveFieldObject(initial, initial, config))
  }

  const setCue = (cue: boolean) => {
    clearCueTimeout()
    setStateWithRef({
      cue,
      cueInvalid: cue && !refState.current.valid
    })
  }

  const setValue = (value: T) => {
    clearCueTimeout()
    let { cue } = refState.current
    if (config.cueDelay === 0) cue = true
    else if (config.cueDelay || 0 > 0) {
      cue = false
      refTimeoutId.current = setTimeout(() => {
        setCue(true)
      }, config.cueDelay)
    }
    setStateWithRef(deriveFieldObject(initial, value, config, cue))
  }

  useEffect(() => {
    if (refMounted.current) setStateWithRef(deriveFieldObject(initial, state.value, config, refState.current.cue))
  }, config.deps || [])

  useEffect(() => {
    if (refMounted.current) restore()
    refMounted.current = true
    return clearCueTimeout
  }, [initial])

  return {
    ...state,
    restore,
    setCue,
    setValue
  }
}

// --------------------------------------------------
// Hook Helpers
// --------------------------------------------------
const deriveFieldObject = <T, P extends IFieldValidationConfigBase<T>>(
  initial: T,
  value: T,
  // Remove base rules from config to leave us with custom config
  { cueDelay, deps, label, required, rules, ...customConfig }: P,
  cue = false
): IFieldObjectCustomConfig<T, P> & IFieldValidationStateBase<T> => {
  const errors = validate(value, { label, required, rules })
  const valid = errors.length === 0
  const stateValidate = {
    changed: initial !== value,
    cue,
    cueInvalid: cue && !valid,
    errors,
    label: label || "",
    required: !!required,
    valid,
    value,
    ...customConfig
  }
  return stateValidate
}

const validate = <T>(value: T, props: IFieldValidationConfigBase<T>) => {
  const label = props.label || "Field"
  return [
    [
      (value: T) => !props.required || (value as number) === 0 || !!value,
      (typeof props.required !== "boolean" && props.required) || `${label} is required`
    ] as [(value: T) => boolean, string],
    ...(props.rules || [])
  ]
    .map(([cb, message]) => !cb(value) && (message || `${label} is invalid`))
    .filter(Boolean) as string[]
}

// --------------------------------------------------
// Field Bag Utils (Tree Shakable)
// --------------------------------------------------
// >[TEST]<:
export const fieldBagChanged = <T, P = {}>(fieldBag: object) => Object.values(fieldBag).every(({ changed }) => changed)

export const fieldBagValid = <T, P = {}>(fieldBag: object) => Object.values(fieldBag).every(({ valid }) => valid)

export const fieldBagReduceToValues = <T, F extends object>(fieldBag: F) =>
  Object.entries(fieldBag).reduce((acc, [key, field]) => {
    acc[key] = field.value
    return acc
  }, {} as any) as { [key in keyof F]: string }

export const fieldBagTraverse = <T, P = {}>(fieldBag: object, cb: (field: IFieldObject<T, P>) => void) =>
  Object.values(fieldBag).forEach(cb)

// --------------------------------------------------
// Validation Functions (Tree Shakable)
// --------------------------------------------------
export const ruleEmail = () => (value: string) =>
  // Source: https://www.abstractapi.com/tools/email-regex-guide
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)

export const ruleEnum = (list: string[]) => (value: string) => list.indexOf(value) > -1

export const ruleLength =
  ({ min, max }: { min?: number; max?: number }) =>
  (value: string) =>
    (min || 0) <= value.length && value.length <= (max || Number.MIN_VALUE)

export const ruleMatch = (pattern: RegExp) => (value: string) => pattern.test(value)

export const ruleNumeric = () => (value: string) => !isNaN(value as any)

export const rulePhone = () =>
  // Source: https://ihateregex.io/expr/phone/
  ruleMatch(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/)

export const ruleZip = () =>
  // Source: https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s14.html
  ruleMatch(/^[0-9]{5}(?:-[0-9]{4})?$/)

// --------------------------------------------------
// Misc Utils
// --------------------------------------------------
export const defineConfig = <T, P = {}>(rule: IFieldValidationConfig<T, P>): IFieldValidationConfig<T, P> => rule

// --------------------------------------------------
// Default Export
// --------------------------------------------------
export default useStateValidate
