import { act, renderHook } from "@testing-library/react-hooks"
import useStateValidate from "../index"

describe("Value", () => {
  it("should have initial value", () => {
    const { result } = renderHook(() => useStateValidate("Ray Finkle", {}))
    expect(result.current.value).toEqual("Ray Finkle")
  })

  it("should update the value", async () => {
    await act(async () => {
      const { result, waitForNextUpdate } = renderHook(() => useStateValidate("Apple", {}))
      result.current.setValue("Orange")
      await waitForNextUpdate()
      expect(result.current.value).toEqual("Orange")
    })
  });
})
