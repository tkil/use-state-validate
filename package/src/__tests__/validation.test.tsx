import { renderHook } from "@testing-library/react-hooks"
import useStateValidate, { ruleLength, ruleMatch } from "../index"

describe("Validation", () => {
  it("should show valid when required", () => {
    const { result } = renderHook(() => useStateValidate("I follow rules", { required: true }))
    expect(result.current.valid).toEqual(true)
    expect(result.current.errors.length).toEqual(0)
  })

  it("should show invalid when required but missing", () => {
    const { result } = renderHook(() => useStateValidate("", { required: true }))
    expect(result.current.valid).toEqual(false)
    expect(result.current.errors[0]).toEqual("Field is required")
  })

  it("should show two error messages", () => {
    const { result } = renderHook(() =>
      useStateValidate("letmein", {
        rules: [
          [ruleLength({ min: 8 }), "Too short!"],
          [ruleMatch(/^(?=.*[a-z])(?=.*[A-Z]).*/), "Use and upper and lower!"]
        ]
      })
    )
    expect(result.current.valid).toEqual(false)
    expect(result.current.errors.length).toEqual(2)
  })

  it("should show three error messages", () => {
    const { result } = renderHook(() =>
      useStateValidate("", {
        required: true,
        rules: [
          [ruleLength({ min: 8 }), "Too short!"],
          [ruleMatch(/^(?=.*[a-z])(?=.*[A-Z]).*/), "Use and upper and lower!"]
        ]
      })
    )
    expect(result.current.valid).toEqual(false)
    expect(result.current.errors.length).toEqual(3)
  })
})
