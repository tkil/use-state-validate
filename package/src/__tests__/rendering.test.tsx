import React from "react";
import { render } from "@testing-library/react";
import useStateValidate from "../index";

describe("Rendering", () => {
  it("should not break a component", () => {
    // Arrange
    const Component: React.FC = () => {
      useStateValidate("", {});
      return <div></div>;
    };
    const { baseElement } = render(<Component />);
    // Act
    const element = baseElement.firstChild;
    // Assert
    expect(element).not.toBeNull();
  });
});
