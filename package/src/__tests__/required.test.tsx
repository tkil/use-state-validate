import { renderHook } from "@testing-library/react-hooks"
import useStateValidate, { ruleMatch } from "../index"

describe("Validation", () => {
  describe("Flag", () => {
    it("should not show require by default", () => {
      const { result } = renderHook(() => useStateValidate("", {}))
      expect(result.current.required).toEqual(false)
    })

    it("should flag required when set", () => {
      const { result } = renderHook(() => useStateValidate("", { required: true }))
      expect(result.current.required).toEqual(true)
    })

    it("should show invalid when required is true", () => {
      const { result } = renderHook(() => useStateValidate("", { required: true }))
      expect(result.current.valid).toEqual(false)
    })

    it("should show valid when required false", () => {
      const { result } = renderHook(() => useStateValidate("", { required: false }))
      expect(result.current.valid).toEqual(true)
    })

    it("should show invalid when required and has matcher", () => {
      const { result } = renderHook(() => useStateValidate("", { required: true, rules: [[ruleMatch(/foo/)]] }))
      expect(result.current.valid).toEqual(false)
    })
  })
  describe("Message", () => {
    it("should show Field is Required", () => {
      const { result } = renderHook(() => useStateValidate("", { required: true }))
      expect(result.current.errors[0]).toEqual("Field is required")
    })

    it("should show First Name is Required", () => {
      const { result } = renderHook(() => useStateValidate("", { label: "First Name", required: true }))
      expect(result.current.errors[0]).toEqual("First Name is required")
    })

    it("should show custom message", () => {
      const { result } = renderHook(() => useStateValidate("", { label: "First Name", required: "You missed me" }))
      expect(result.current.errors[0]).toEqual("You missed me")
    })
  })
})
