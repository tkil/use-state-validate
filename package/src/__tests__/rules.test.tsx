import { renderHook } from "@testing-library/react-hooks"
import useStateValidate, { ruleEnum, ruleLength, ruleMatch } from "../index"

describe("Function", () => {
  describe("Custom", () => {
    it("should error when fn returns false", () => {
      const { result } = renderHook(() =>
        useStateValidate("red", {
          rules: [[(val: string) => val === "green", "The function likes green"]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("The function likes green")
    })

    it("should not error when fn returns true", () => {
      const { result } = renderHook(() =>
        useStateValidate("green", {
          rules: [[(val: string) => val === "green", "The function likes green"]]
        })
      )
      expect(result.current.errors.length).toEqual(0)
    })

    it("should do nothing when rules are empty array", () => {
      const { result } = renderHook(() =>
        useStateValidate("green", {
          rules: []
        })
      )
      expect(result.current.errors.length).toEqual(0)
    })

    it("should show multiple errors when failing multiple functions", () => {
      const { result } = renderHook(() =>
        useStateValidate("yellow", {
          rules: [
            [(val: string) => val === "red", "The function likes red"],
            [(val: string) => val === "green", "The function likes green"],
            [(val: string) => val === "blue", "The function likes blue"]
          ]
        })
      )

      expect(result.current.errors[0]).toEqual("The function likes red")
      expect(result.current.errors[1]).toEqual("The function likes green")
      expect(result.current.errors[2]).toEqual("The function likes blue")
    })
  })

  describe("Enum", () => {
    it("should show error message for incorrect enum", () => {
      const { result } = renderHook(() =>
        useStateValidate("bird", {
          rules: [[ruleEnum(["cat", "dog", "fish"]), "Field must be either cat, dog or fish"]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("Field must be either cat, dog or fish")
    })

    it("should show error message for incorrect enum with custom error", () => {
      const { result } = renderHook(() =>
        useStateValidate("bird", {
          rules: [[ruleEnum(["cat", "dog"]), "You dummy!"]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("You dummy!")
    })

    it("should show error message for incorrect enum with custom error", () => {
      const { result } = renderHook(() =>
        useStateValidate("bird", {
          rules: [[ruleEnum(["cat", "dog"]), "You dummy!"]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("You dummy!")
    })

    it("should show valid for correct enum", () => {
      const { result } = renderHook(() =>
        useStateValidate("dog", {
          rules: [[ruleEnum(["cat", "dog"]), "You dummy!"]]
        })
      )
      expect(result.current.errors.length).toEqual(0)
    })
  })

  describe("Length", () => {
    it("should show invalid message when too short", () => {
      const { result } = renderHook(() =>
        useStateValidate("A", {
          rules: [[ruleLength({ min: 3 }), "Field must be at least 3 characters"]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("Field must be at least 3 characters")
    })

    it("should show invalid message when too long", () => {
      const { result } = renderHook(() =>
        useStateValidate("Hubert Blaine Wolfeschlegelsteinhausenbergerdorff Sr", {
          label: "Name",
          rules: [[ruleLength({ max: 20 })]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("Name is invalid")
    })

    it("should show invalid message when below range", () => {
      const { result } = renderHook(() =>
        useStateValidate("ab", {
          label: "Thing",
          rules: [[ruleLength({ min: 3, max: 8 })]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("Thing is invalid")
    })

    it("should show invalid message when above range", () => {
      const { result } = renderHook(() =>
        useStateValidate("123456789", {
          rules: [[ruleLength({ min: 3, max: 8 }), "Field must be between 3-8 characters"]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("Field must be between 3-8 characters")
    })

    it("should show valid message when inside range", () => {
      const { result } = renderHook(() =>
        useStateValidate("123", {
          rules: [[ruleLength({ min: 3, max: 8 }), "Field must be between 3-8 characters"]]
        })
      )
      expect(result.current.errors.length).toEqual(0)
    })

    it("should show custom length invalid message", () => {
      const { result } = renderHook(() =>
        useStateValidate("123456789", {
          rules: [[ruleLength({ min: 3, max: 8 }), "That length is all wrong..."]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("That length is all wrong...")
    })
  })

  describe("Match", () => {
    it("should show invalid when no match", () => {
      const { result } = renderHook(() =>
        useStateValidate("red", {
          rules: [[ruleMatch(/[0-9]/), "Should container a number"]]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual("Should container a number")
    })

    it("should show multiple errors", () => {
      const { result } = renderHook(() =>
        useStateValidate("letmein", {
          required: true,
          rules: [
            [ruleMatch(/[0-9]/), "Should container a number"],
            [ruleMatch(/[^A-z\s\d][\\\^]?/), "Should container special character"]
          ]
        })
      )
      expect(result.current.errors.length).toEqual(2)
      expect(result.current.errors[0]).toEqual("Should container a number")
      expect(result.current.errors[1]).toEqual("Should container special character")
    })

    it.each([
      [1, "letmein123$", "Should container upper case letter"],
      [2, "letmeinABC$", "Should container a number"],
      [3, "letmeinABC123", "Should container special character"]
    ])("should have one error for match rule %s", (_, value, message) => {
      const { result } = renderHook(() =>
        useStateValidate(value, {
          required: true,
          rules: [
            [ruleMatch(/[A-Z]/), "Should container upper case letter"],
            [ruleMatch(/[0-9]/), "Should container a number"],
            [ruleMatch(/[^A-z\s\d][\\\^]?/), "Should container special character"]
          ]
        })
      )
      expect(result.current.errors.length).toEqual(1)
      expect(result.current.errors[0]).toEqual(message)
    })

    it("should have no errors", () => {
      const { result } = renderHook(() =>
        useStateValidate("letmein90$&^%", {
          required: true,
          rules: [
            [ruleMatch(/[0-9]/), "Should container a number"],
            [ruleMatch(/[^A-z\s\d][\\\^]?/), "Should container special character"]
          ]
        })
      )
      expect(result.current.errors.length).toEqual(0)
    })
  })
})
