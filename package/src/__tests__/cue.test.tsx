import { waitFor } from "@testing-library/react"
import { act, renderHook } from "@testing-library/react-hooks"

import useStateValidate from "../index"

describe("Cue", () => {
  it("should not Cue by default", () => {
    const { result } = renderHook(() => useStateValidate("", {}))
    expect(result.current.cue).toEqual(false)
  })

  it("should Cue when set", async () => {
    await act(async () => {
      const { result, waitForNextUpdate } = renderHook(() => useStateValidate("", {}))
      result.current.setCue(true)
      await waitForNextUpdate()
      expect(result.current.cue).toEqual(true)
    })
  })

  it("should not Cue when value is set by default", async () => {
    await act(async () => {
      const { result, waitForNextUpdate } = renderHook(() => useStateValidate("apple", {}))
      result.current.setValue("orange")
      await waitForNextUpdate()
      expect(result.current.cue).toEqual(false)
    })
  })

  it("should Cue when value is set and cueDelay is 0", async () => {
    await act(async () => {
      const { result, waitForNextUpdate } = renderHook(() => useStateValidate("apple", { cueDelay: 0 }))
      result.current.setValue("orange")
      await waitForNextUpdate()
      expect(result.current.cue).toEqual(true)
    })
  })

  it("should Cue with a delay when Cue delay is set", async () => {
    await act(async () => {
      // Arrange
      jest.useFakeTimers()
      const { result } = renderHook(() => useStateValidate("apple", { cueDelay: 2000 }))
      // Act
      result.current.setValue("orange")
      jest.advanceTimersByTime(2100)
      // Assert
      await waitFor(() => {
        expect(result.current.cue).toEqual(true)
      })
      // Cleanup
      jest.useRealTimers()
    })
  })
})
