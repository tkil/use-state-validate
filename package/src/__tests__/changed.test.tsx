import { renderHook, act } from "@testing-library/react-hooks"
import useStateValidate from "../index"

describe("Changed", () => {
  it("should not be changed default", () => {
    const { result } = renderHook(() => useStateValidate("squeaky clean", {}))
    expect(result.current.changed).toEqual(false)
  })

  it("should be changed when the field is updated", async () => {
    await act(async () => {
      // Arrange
      const { result, waitForNextUpdate } = renderHook(() => useStateValidate("squeaky clean", {}))
      // Act
      result.current.setValue("soiled")
      await waitForNextUpdate()
      // Assert
      expect(result.current.changed).toEqual(true)
    })
  })

  it("should not be changed when returned to initial value", async () => {
    await act(async () => {
      // Arrange
      const { result, waitForNextUpdate } = renderHook(() => useStateValidate("Foo", {}))
      // Act
      result.current.setValue("Bar")
      result.current.setValue("Foo")
      await waitForNextUpdate()
      // Assert
      expect(result.current.changed).toEqual(false)
      expect(result.current.value).toEqual("Foo")
    })
  })

  it("should not be changed when re-rendered to new value", async () => {
    await act(async () => {
      // Arrange
      const { result, waitForNextUpdate, rerender } = renderHook<{ initialValue: string }, any>(
        ({ initialValue }) => useStateValidate(initialValue, {}),
        { initialProps: { initialValue: "Foo" } }
      )
      // Act
      result.current.setValue("Bar")
      await waitForNextUpdate()
      rerender({ initialValue: "Biz" })
      await waitForNextUpdate()
      // Assert
      expect(result.current.changed).toEqual(false)
    })
  })
})
