import { renderHook } from "@testing-library/react-hooks"
import useStateValidate from "../index"

describe("Label", () => {
  it.each(["First Name", "Last Name", "Email"])("should show the label %s", (label) => {
    const { result } = renderHook(() => useStateValidate("Ray Finkle", { label }))
    expect(result.current.label).toEqual(label)
  })
})
