module.exports = {
  clearMocks: true,
  collectCoverage: false,
  collectCoverageFrom: ["!**/lib/**"],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: ["/node_modules/"],
  globals: {
    "ts-jest": {
      tsconfig: "tsConfig.mjs.json",
    },
  },
  moduleFileExtensions: ["ts", "tsx", "js", "json"],
  moduleNameMapper: {
    "@modules/(.*)": "<rootDir>/node_modules/$1",
    "@utils/(.*)": "<rootDir>/src/utils/$1",
  },
  modulePathIgnorePatterns: [],
  testMatch: ["**/index.test.tsx", "**/__tests__/*test.+(ts|tsx|js)", "**/tests/integration/*test.+(ts|tsx|js)"],
  testPathIgnorePatterns: ["/node_modules/"],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
  },
};
